from .models import User
import datetime


class UserLastIpLogger:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            try:
                user = User.objects.get(id=request.user.id)
                user.last_request_at = datetime.datetime.now()
                user.save()
            except User.DoesNotExist:
                pass

        response = self.get_response(request)

        return response
