from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        "username",
        "get_full_name",
        "email",
        "phone",
        "is_active",
        "is_staff",
    )
    list_display_links = ("username",)
    list_editable = ("is_active",)


admin.site.site_title = _("Social Network Dashboard")
admin.site.site_header = _("Social Network Dashboard")
