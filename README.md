# App "Social Network with DRF"

<a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
* Link to api working api: https://immense-sierra-99208.herokuapp.com/api/v1/posts/
* Postman collection: https://www.getpostman.com/collections/0ea18ea887cd68d324bb
* You need add header "JWT" in Postman app with value Token {token_that_you_take_from_login}
* Collection consist of variables like api_v1_url and all requests than you can do
##Local development
To run app in localhost you need to perform in work dir
```bash
$ git clone https://gitlab.com/danil_vorvul/django-social-network.git
```
Than go to project dir, create virtual environment
```bash
$ cd django-social-network
$ python3.7 -m venv env
```
Activate virtual enviroment and install dependencies
```bash
$ source env/bin/activate
$ pip install -r requirements.txt
```
Now you need to make migrations and migrate
```bash
$ python manage.py makemigrations
$ python manage.py migrate
```
``Finally you can run app into http://localhost:8000
```bash
$ python manage.py runserver
```

##Push changes to Heroku
Connect to Heroku and create new app
```bash
$ heroku login
$ heroku create
$ heroku container:login
```
You need to rebuilt Docker container, go to project root and run:
```
$ docker build -t registry.heroku.com/immense-sierra-99208/web .
```
Push your changes
```
$ docker push registry.heroku.com/immense-sierra-99208/web
```
Now release container to Heroku
```bash
$ heroku container:release -a immense-sierra-99208  web
```
Go to Heroku Dashboard and add Postgresql addon to your app, than apply migrations
```bash
$ heroku run python manage.py makemigrations -a immense-sierra-99208
$ heroku run python manage.py migrate -a immense-sierra-99208
```
App have been successfully deployed)