from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from .yasg import urlpatterns as doc_urls

urlpatterns = [
    path("admin/", admin.site.urls),
    url("api-auth/", include("rest_framework.urls")),
    url("authorize/", include("djoser.urls.jwt")),
    url("auth/", include("djoser.urls")),
    url("api/v1/", include("app.api.v1.urls")),
]

urlpatterns += doc_urls

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
