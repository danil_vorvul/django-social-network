from rest_framework.routers import DefaultRouter

from .views import UserLastActivityViewSet


router = DefaultRouter()
router.register(r"user/last-activity", UserLastActivityViewSet, basename="posts")
urlpatterns = router.urls
