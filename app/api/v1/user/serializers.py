from rest_framework import serializers

from user.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "username",
            "id",
        )


class UserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("last_login", "last_request_at")
