from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework import mixins

from user.models import User
from .serializers import UserActivitySerializer


class UserLastActivityViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = UserActivitySerializer
    queryset = User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        if request.user.is_staff is False:
            if kwargs["pk"] != request.user.id:
                return Response(status=403)

        return super().retrieve(request, args, kwargs)
