from rest_framework import serializers

from post.models import Post, PostLike
from app.api.v1.user.serializers import UserSerializer
from app.api.v1.comment.serializers import CommentSerializer


class PostListSerializer(serializers.ModelSerializer):
    """List of posts with comments"""

    user = UserSerializer()
    rating = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = ["id", "user", "title", "content", "rating"]


class PostCUDSerializer(serializers.ModelSerializer):
    """Create, Update, Delete serializer"""

    class Meta:
        model = Post
        fields = ["title", "content", "user"]


class PostDetailSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)
    user = UserSerializer()
    rating = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        fields = ["id", "user", "title", "comments", "content", "rating"]


class LikesAnalyticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostLike
        fields = "__all__"
