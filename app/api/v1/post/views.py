from django.shortcuts import get_object_or_404
from django.db.models import Count

from rest_framework import permissions, viewsets, mixins
from rest_framework.response import Response
from rest_framework.views import APIView

from post.models import Post, PostLike
from .permissions import IsOwnerOrReadOnly
from .serializers import (
    PostCUDSerializer,
    PostListSerializer,
    PostDetailSerializer,
    LikesAnalyticsSerializer,
)
from .paginations import PostApiPagination, LikeApiPagination


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    pagination_class = PostApiPagination

    def get_serializer_class(self):
        if self.action == "list":
            return PostListSerializer
        elif (
            self.action == "create"
            or self.action == "delete"
            or self.action == "update"
            or self.action == "partial_update"
        ):
            return PostCUDSerializer
        else:
            return PostDetailSerializer

    def get_queryset(self, pk=None):
        return Post.objects.filter().annotate(rating=Count("likes"))

    def get_permissions(self):
        if self.action == "list" or self.action == "retrieve":
            self.permission_classes = [permissions.AllowAny]
        elif self.action == "create":
            self.permission_classes = [permissions.IsAuthenticated]
        elif self.action == "update" or self.action == "partial_update":
            self.permission_classes = [IsOwnerOrReadOnly]
        elif self.action == "destroy":
            self.permission_classes = [permissions.IsAdminUser]
        return super().get_permissions()


class AddOrDeleteLikeAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, pk):
        post = get_object_or_404(Post, id=pk)
        PostLike.like_or_unlike(post, request.user)
        return Response(status=201)


class LikesAnalyticsViewset(viewsets.GenericViewSet, mixins.ListModelMixin):
    permission_classes = [permissions.IsAdminUser]
    pagination_class = LikeApiPagination
    queryset = PostLike.objects.all()
    serializer_class = LikesAnalyticsSerializer

    def list(self, request, *args, **kwargs):
        try:
            self.queryset = PostLike.get_user_likes_in_date_range(
                user=request.user,
                date_from=request.GET.get("date_from"),
                date_to=request.GET.get("date_to"),
            )
        except ValueError:
            return Response({"error": "invalid data"}, status=400)
        return super(LikesAnalyticsViewset, self).list(request, args, kwargs)
