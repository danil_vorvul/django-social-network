from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views


urlpatterns = [
    path(
        "posts/<int:pk>/like/",
        views.AddOrDeleteLikeAPIView.as_view(),
        name="Adding or deleting vote if it exists",
    ),
]

router = DefaultRouter()
router.register(r"posts", views.PostViewSet, basename="posts")
router.register(r"posts/analytics/likes", views.LikesAnalyticsViewset, basename="likes")
urlpatterns += router.urls
