import time
from datetime import datetime, timedelta
from django.db import models

from user.models import User


class Post(models.Model):
    """Users post"""

    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    title = models.CharField(max_length=300)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created_at"]

    def __str__(self):
        return self.title


class PostLike(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="likes")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    @staticmethod
    def get_user_likes_in_date_range(user, date_from, date_to):
        date_from, date_to = PostLike.validate_date_range(date_from, date_to)
        return PostLike.objects.filter(
            user=user, created_at__date__range=(date_from, date_to)
        )

    @staticmethod
    def validate_date_range(date_from, date_to):
        if date_to is None:
            date_to = datetime.today().date()

        if date_from is None:
            date_from = date_to - timedelta(days=1)

        # this two lines will raise ValueError if data invalid
        time.strptime(str(date_to), "%Y-%m-%d")
        time.strptime(str(date_from), "%Y-%m-%d")

        return date_from, date_to

    @staticmethod
    def like_or_unlike(post, user):
        """Add like or delete existing"""
        try:
            likes_logger = PostLike.objects.get(post=post, user=user)
            likes_logger.delete()
        except PostLike.DoesNotExist:
            likes_logger = PostLike(post=post, user=user)
            likes_logger.save()
